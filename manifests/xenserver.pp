class cron::xenserver inherits cron::base {
  Package['cron'] {
    name => 'vixie-cron'
  }
  Service['cron'] {
    name => 'crond'
  }
}
